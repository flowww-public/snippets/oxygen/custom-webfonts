<?php
namespace oxy\fonts {

	class Webfonts {

		private string $ENQUEUE_DIR = 'oxy';
		private string $ENQUEUE_DIR_FONT = 'webfonts';
		private string $ENQUEUE_FILE = 'fonts.css';
		private string $ENQUEUE_NAME = 'oxy-webfonts';

		/**
		 * Instance
		 * 
		 * @var Webfonts|null
		 */
		private static ?Webfonts $instance = null;

		/**
		 * Singleton
		 *
		 * @return Webfonts|null
		 */
		public static function get_instance(): Webfonts {

			if(self::$instance == null) {

				self::$instance = new self();
				self::$instance->init();

			}

			return self::$instance;

		}

		/**
		 * Initialize Singleton
		 */
		private function init(): void {

			$this->add_acf_page();
			$this->add_acf_fields();

			\add_filter( 'upload_mimes',			[ $this, 'on_upload_mimes'] );
			\add_filter( 'acf/save_post',			[ $this, 'on_save' ], 10, 4 );
			\add_action( 'wp_enqueue_scripts', 		[ $this, 'on_enqueue' ], -100 );
			\add_action( 'init', 					[ $this, 'on_init' ], 3 );

		}

		/**
		 * Initialize Hook
		 */
		public function on_init(): void {
			
			\add_action( 'ct_builder_ng_init',          [ $this, 'on_ct_builder_ng_init' ] );

		}


		/**
		 * Show Fonts in Oxygen
		 */
		public function on_ct_builder_ng_init(): void {

			$font_family_list = [];

			$sets = \get_field( 'options_webfonts_fonts_sets', 'options' );

			foreach($sets as $set) {

				$font_face = $set['options_webfonts_fonts_set_face'];

				if( $set[ 'options_webfonts_fonts_set_fallback' ] != "" ) {

					$font_face = $font_face.', '.$set[ 'options_webfonts_fonts_set_fallback' ];

				}

				$font_family_list[] = $font_face;

			}

			$font_family_list = array_unique( $font_family_list );
			$output = json_encode( $font_family_list );
			$output = htmlspecialchars( $output, ENT_QUOTES );
			echo "elegantCustomFonts=$output;";

		}

		/**
		 * Enable Upload For Font Mime Types
		 *
		 * @param array $mimes
		 *
		 * @return array
		 */
		public function on_upload_mimes( array $mimes ): array {

			$mimes['otf'] = 'application/x-font-otf';
			$mimes['woff'] = 'application/x-font-woff';
			$mimes['woff2'] = 'application/octet-stream';
			$mimes['ttf'] = 'application/x-font-ttf';
			$mimes['eot'] = 'application/vnd.ms-fontobject';

			return $mimes;

		}

		/**
		 * Enqueues CSS File
		 */
		public function on_enqueue(): void {

			$enqueue_dir_id = '1';

			if( is_multisite() ) {

				$enqueue_dir_id = \get_current_blog_id();

			}

			$upload_dir = \wp_upload_dir();
			$url = $upload_dir[ 'baseurl' ] .
			       '/' . $this->ENQUEUE_DIR .
			       '/' . $enqueue_dir_id .
			       '/' . $this->ENQUEUE_DIR_FONT .
			       '/' . $this->ENQUEUE_FILE;

			\wp_enqueue_style( $this->ENQUEUE_NAME, $url );

		}

		/**
		 * Saves CSS File to Uploads Folder
		 */
		public function on_save(): void {

			$screen = \get_current_screen();

			if ( strpos( $screen->id, 'oxy_options_webfonts' ) == true ) {

				$css = self::minify_css( $this->get_fonts_css() );

				$enqueue_dir_id = '1';

				if( \is_multisite() ) {

					$enqueue_dir_id = \get_current_blog_id();

				}

				$upload_dir = \wp_upload_dir();

				$filepath = $upload_dir[ 'basedir' ] . '/' .
				            $this->ENQUEUE_DIR . '/' .
				            $enqueue_dir_id . '/' .
				            $this->ENQUEUE_DIR_FONT;

				if ( !is_dir( $filepath ) ) {

					mkdir( $filepath,  0775, true );

				}

				file_put_contents( $filepath . '/' . $this->ENQUEUE_FILE, $css );

			}

		}

		/**
		 * Get CSS By Field Name
		 *
		 * @return string
		 */
		private function get_fonts_css(): string {

			$css = '';

			$sets = \get_field( 'options_webfonts_fonts_sets', "options" );
			foreach($sets as $set) {

				$font_face = $set[ 'options_webfonts_fonts_set_face' ];
				$font_style = $set[ 'options_webfonts_fonts_set_style' ];
				$font_weight = $set[ 'options_webfonts_fonts_set_weight' ];
				$font_display = $set[ 'options_webfonts_fonts_set_display' ];

				$src = [];

				if( !empty( $set['options_webfonts_fonts_set_file_eot'] ) ) {

					array_push($src, 'url(' .
					                 \wp_get_attachment_url( $set[ 'options_webfonts_fonts_set_file_eot' ] ) .
					                 ')'
					);

				}

				if( !empty( $set['options_webfonts_fonts_set_file_woff'] ) ) {

					array_push($src, 'url(' .
					                 \wp_get_attachment_url( $set[ 'options_webfonts_fonts_set_file_woff'] ) .
					                 ') format("woff")'
					);

				}

				if( !empty( $set['options_webfonts_fonts_set_file_woff2'] ) ) {

					array_push($src, 'url(' .
					                 \wp_get_attachment_url($set['options_webfonts_fonts_set_file_woff2'] ) .
					                 ') format("woff2")' );

				}

				if( !empty( $set['options_webfonts_fonts_set_file_ttf'] ) ) {

					array_push($src, 'url(' .
					                 \wp_get_attachment_url( $set['options_webfonts_fonts_set_file_ttf'] ) .
					                 ') format("truetype")'
					);

				}

				if( count( $src ) > 0 ) {

					$src = implode( ",", $src );

					$css .= '
						@font-face {
							font-family: "' . $font_face . '";
							src: ' . $src . ';
							font-weight: ' . $font_weight . ';
							font-style: ' . $font_style . ';
							font-display: ' . $font_display . ';
						}
					';

				}

			}

			return $css;

		}

		/**
		 * Adds ACF Admin Options Page
		 */
		private function add_acf_page(): void {

			if( function_exists('acf_add_options_page') ) {

				\acf_add_options_page(
					[
						'menu_title'	    =>	'Webfonts',
						'page_title' 	    =>	'Webfonts',
						'menu_slug'		    =>	'oxy_webfonts',
						'parent_slug'       =>  'themes.php',
						'capability' 	    =>  'manage_options',
						'redirect'		    => 	true,
						'icon_url'          =>  'dashicons-admin-customizer',
						'update_button'     =>  'Schriften speichern',
						'updated_message'   =>  'Schriften gespeichert',
					]
				);

			}

		}

		/**
		 * Adds ACF Admin Fields
		 */
		private function add_acf_fields(): void {

			if ( function_exists( 'acf_add_local_field_group' ) ) {

				\acf_add_local_field_group(array(
					'key' => 'group_60670f1345782',
					'title' => 'Fonts',
					'fields' => array(
						array(
							'key' => 'field_60670f171d9cb',
							'name' => 'options_webfonts_fonts_sets',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'collapsed' => '',
							'min' => 0,
							'max' => 0,
							'layout' => 'block',
							'button_label' => 'Schrift hinzufügen',
							'sub_fields' => array(
								array(
									'key' => 'field_60670f371d9cc',
									'label' => 'Font Face',
									'name' => 'options_webfonts_fonts_set_face',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
								),
								array(
									'key' => 'field_60670f371g9cc',
									'label' => 'Fallback Fonts',
									'name' => 'options_webfonts_fonts_set_fallback',
									'type' => 'text',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
								),
								array(
									'key' => 'field_60670f511d9cd',
									'label' => 'Font Style',
									'name' => 'options_webfonts_fonts_set_style',
									'type' => 'select',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'choices' => array(
										'normal' => 'Normal',
										'italic' => 'Italic',
									),
									'default_value' => 'normal',
									'allow_null' => 0,
									'multiple' => 0,
									'ui' => 0,
									'return_format' => 'value',
									'ajax' => 0,
									'placeholder' => '',
								),
								array(
									'key' => 'field_60670f9a1d9ce',
									'label' => 'Font Weight',
									'name' => 'options_webfonts_fonts_set_weight',
									'type' => 'select',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'choices' => array(
										100 => '100',
										200 => '200',
										300 => '300',
										400 => '400',
										500 => '500',
										600 => '600',
										700 => '700',
										800 => '800',
										900 => '900',
										'normal' => 'Normal',
										'bold' => 'Bold',
										'bolder' => 'Bolder',
										'lighter' => 'lighter',
									),
									'default_value' => 'normal',
									'allow_null' => 0,
									'multiple' => 0,
									'ui' => 0,
									'return_format' => 'value',
									'ajax' => 0,
									'placeholder' => '',
								),
								array(
									'key' => 'field_606c3ddb3b01e',
									'label' => 'Font Display',
									'name' => 'options_webfonts_fonts_set_display',
									'type' => 'select',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'choices' => array(
										'auto' => 'Auto',
										'block' => 'Block',
										'swap' => 'Swap',
										'fallback' => 'Fallback',
										'optional' => 'Optional',
									),
									'default_value' => 'auto',
									'allow_null' => 0,
									'multiple' => 0,
									'ui' => 0,
									'return_format' => 'value',
									'ajax' => 0,
									'placeholder' => '',
								),
								array(
									'key' => 'field_606710471d9cf',
									'label' => 'WOFF',
									'name' => 'options_webfonts_fonts_set_file_woff',
									'type' => 'file',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'return_format' => '',
									'library' => '',
									'min_size' => '',
									'max_size' => '',
									'mime_types' => 'woff',
								),
								array(
									'key' => 'field_606710691d9d0',
									'label' => 'WOFF2',
									'name' => 'options_webfonts_fonts_set_file_woff2',
									'type' => 'file',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'return_format' => '',
									'library' => '',
									'min_size' => '',
									'max_size' => '',
									'mime_types' => 'woff2',
								),
								array(
									'key' => 'field_606710731d9d1',
									'label' => 'TTF',
									'name' => 'options_webfonts_fonts_set_file_ttf',
									'type' => 'file',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'return_format' => '',
									'library' => '',
									'min_size' => '',
									'max_size' => '',
									'mime_types' => 'ttf',
								),
								array(
									'key' => 'field_6067107f1d9d2',
									'label' => 'EOT',
									'name' => 'options_webfonts_fonts_set_file_eot',
									'type' => 'file',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'return_format' => '',
									'library' => '',
									'min_size' => '',
									'max_size' => '',
									'mime_types' => 'eot',
								),
							),
						),
					),
					'location' => array(
						array(
							array(
								'param' => 'options_page',
								'operator' => '==',
								'value' => 'oxy_webfonts',
							),
						),
					),
					'menu_order' => 0,
					'position' => 'normal',
					'style' => 'seamless',
					'label_placement' => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen' => '',
					'active' => true,
					'description' => '',
				));

			}

		}

		/**
		 * Minifies css
		 *
		 * @param string $input
		 *
		 * @return string
		 */
		private static function minify_css( string $input ): string {

			if( trim( $input ) === "" ) {

				return $input;

			}

			return preg_replace( "/\s\s+/", " ", $input );

		}

	}

	Webfonts::get_instance();

}
